"""
This program reads a temperature in fahrenheit from
the keyboard, converts it to centigrade and prints the results.
Then it requests another input from the keyboard.
"""

while True:
    temp = raw_input('Enter a temperature: ')
    if temp == 'q' or temp == 'Q':  # The variable name must be repeated.
        break
    ftemp = float(temp)
    ctemp = 5.0/9.0*(ftemp-32)
    print '%d degrees Fahrenheit is %.1f degrees Centigrade' % (ftemp, ctemp)
    if ftemp > 100:
        print "It's very hot!"
    elif ftemp > 80:
        print "It's hot."
    elif ftemp > 60:
        print "It's nice out."
    elif ftemp > 40:
        print "It's chilly,"
    else:
        print "It's cold!"

print 'Conversions ended'
