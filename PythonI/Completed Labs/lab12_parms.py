"""
This program reads a temperature in fahrenheit from
the command line, converts it to centigrade (in a function) and prints
the result if the data is valid.
"""

from sys import argv

def fahrenheit_to_centigrade(xtmp):
    nutmp = 5.0 / 9.0 * (xtmp - 32)
    return nutmp

script, temp = argv
try:
    ftemp = float(temp)
except ValueError:
    print 'Input contains non-numeric data - try again'
else:
    ctemp = fahrenheit_to_centigrade(ftemp)
    print '%.1f degrees Fahrenheit is %.1f degrees Centigrade' % (ftemp, ctemp)
    if ftemp > 100:
        print "It's very hot!"
    elif ftemp > 80:
        print "It's hot."
    elif ftemp > 60:
        print "It's nice out."
    elif ftemp > 40:
        print "It's chilly,"
    else:
        print "It's cold!"    
    
