"""
This program reads a temperature in fahrenheit from
the keyboard, converts it to centigrade and prints the results.
"""

temp = raw_input('Enter a temperature: ')
ftemp = float(temp)
ctemp = 5.0/9.0*(ftemp-32)
print '%.1f degrees Fahrenheit is %.1f degrees Centigrade' % (ftemp, ctemp)
if ftemp > 95:
    print "It's very hot!"
elif ftemp > 80:
    print "It's hot."
elif ftemp > 60:
    print "It's nice out."
elif ftemp > 40:
    print "It's chilly,"
else:
    print "It's cold!"
    
