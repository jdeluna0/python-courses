
print "Opening the file..."
# target = open('c:/pydata/treerpt.txt', 'w')
target = open('/home/student/pydata/treerpt.txt', 'w')

line1 = 'This is the first line'
line2 = 'This is the second line'
line3 = 'This is the third line'

print "I'm going to write these to the file."

target.write(line1)
target.write("\n")
target.write(line2)
target.write("\n")
target.write(line3)
target.write("\n")

print "And finally, we close it."
target.close()
