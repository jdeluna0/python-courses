"""
This program reads temperatures in fahrenheit from
a file, converts them to centigrade (in a function) and prints the results.
It stops when the input is an empty string (end of file).
"""
def fahrenheit_to_centigrade(xtmp):
    nutmp = 5.0 /9.0 * (xtmp - 32)
    return nutmp

filein = open('/home/bill/pydata/temps.dat')
# filein = open('c:/pydata/temps.dat')  # Windows file
while True:
    temp = filein.readline()
    if temp == '':
        break
    try:
        ftemp = float(temp)
    except ValueError:
        print 'Input contains non-numeric data - %r' % temp
        continue
    ctemp = fahrenheit_to_centigrade(ftemp)
    print '%.1f degrees Fahrenheit is %.1f degrees Centigrade' % (
        ftemp, ctemp)        
    if ftemp > 100:
        print "It's very hot!"
    elif ftemp > 80:
        print "It's hot."
    elif ftemp > 60:
        print "It's nice out."
    elif ftemp > 40:
        print "It's chilly,"
    else:
        print "It's cold!"  
    
filein.close()
