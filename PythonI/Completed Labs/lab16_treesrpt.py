"""
This program reads a file consisting of tree heights.
It checks the data for validity and the counts the number of trees,
totals the height, determines both the tallest and the shortest trees 
and counts the number of trees over 300 feet.  When done reading the
data, it calculates the average height of a tree in float format and
prints all the results formatted neatly.  Then, it creates a report
file with the same information.

Note to Windows folks - I have left the open statements for Windows
in place as comments.  Note the forward slashes.  Python will take care
of this for you.  Do not use back slashes.  
"""

count = 0
total = 0
tallest = 0
shortest = 1000
over300 = 0
filein = open('c:/pydata/trees.dat', 'r')
# filein = open('/home/student/pydata/trees.dat', 'r')
while True:
    linein = filein.readline()
    if linein == '':  # check for end of file.
        break         # exit the loop.
    try:
        height = float(linein)
    except ValueError:
        print 'Bad Data', linein
        continue    # go back to the beginning of the loop.
    count = count + 1
    total = total + height
    if height < shortest:
        shortest = height
    if height > tallest:
        tallest = height
    if height > 300:
        over300 = over300 + 1
        
filein.close()
avg = total/count  
print '\n\n'  # separate the report from the bad data by a couple of blank lines.
print 'TREE REPORT\n'.center(28) # center is a string method and can be
                                 # used with a string literal.
print '%-21s%7d' % ('Total Trees',count) # The - in the format forces left justification
print '%-21s%7.1f' % ('Average Height',avg)
print '%-21s%7d' % ('Tallest Tree',tallest)
print '%-21s%7d' % ('Shortest Tree',shortest)
print '%-21s%7d' % ('Over 300 Feet',over300)

"""
For all subsequent writes, notice we are basically using the same material
from above.  We just added \n's to force line feeds.  Above, the print
command does this for us automatically.
"""

fileout = open('c:/pydata/treerpt.rtf', 'w')
# fileout = open('/home/bill/pydata/treerpt', 'w')
fileout.write('TREE REPORT'.center(28)) 
fileout.write('\n\n')                   
fileout.write('%-21s%7d\n' % ('Total Trees',count)) 
fileout.write('%-21s%7.1f\n' % ('Average Height',avg))
fileout.write('%-21s%7d\n' % ('Tallest Tree',tallest))
fileout.write('%-21s%7d\n' % ('Shortest Tree',shortest))
fileout.write('%-21s%7d\n' % ('Over 300 Feet',over300))
fileout.close()
    
        

