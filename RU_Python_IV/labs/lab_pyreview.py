#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import random
import string
import requests
import json

app_id = 'e6b1351c'
app_key = '5649ca7ee4509eb1c5aead1f62ac0165'
language = 'en'
"""

:mod:`lab_pyreview` -- Python review
=========================================

LAB PyReview Learning Objective: Review the topics from the previous courses

a. Load the data from the two dictionary files in the data directory into two
   list objects.  data/dictionary1.txt data/dictionary2.txt
   Print the number of entries in each list of words from the dictionary files.

b. Use sets in Python to merge the two lists of words with no duplications (union).
   Print the number of words in the combined list.

c. Import the random library and use one of the functions to print out five random
   words from the combined list of words.

d. Use a list comprehension to find all the words that start with the letter 'a'.
   Print the number of words that begin with the letter 'a'.

e. Create a function called wordcount() with a yield that takes the list of
   all words as an argument and yields a tuple of
   (letter, number_of_words_starting_with_that_letter) with each iteration.

"""

data1 = os.path.join(os.path.dirname(__file__), '../data/dictionary1.txt')
data2 = os.path.join(os.path.dirname(__file__), '../data/dictionary2.txt')


def wordcount(dlist):
    for letter in string.ascii_lowercase:
        count = 0
        for element in dlist:
            if element.startswith(letter):
                count += 1
        yield (letter, count)


with open(data1, "r") as f:
    dlist1 = [line.strip() for line in f.readlines()]

with open(data2, "r") as f:
    dlist2 = [line.strip() for line in f.readlines()]


a = set.union(set(dlist1), set(dlist2))
print("Number of unique words in combined list: {}\n".format(len(a)))


b = list(a)
print("5 Random Words")
for i in range(0, 5):
    word_id = random.choice(b)
    url = 'https://od-api.oxforddictionaries.com:443/api/v1/entries/' + \
            language + '/' + word_id.lower()
    r = requests.get(url, headers={'app_id': app_id, 'app_key': app_key})
    if r.status_code == 404:
        def2 = "Definition not found."
    else:
        def1 = json.loads(r.content)
        try:
            def2 = def1['results'][0]['lexicalEntries'][0]['entries'][0][
                        'senses'][0]['definitions'][0]
        except Exception as e:
            print(r.content)
    print("{} : {}".format(word_id, def2))

count = 0
for element in b:
    if element.startswith('a'):
        count += 1
print("\nNumber of words that start with a: {}\n".format(count))

for i in wordcount(b):
    print("Number of words that start with {} : {}".format(i[0], i[1]))
