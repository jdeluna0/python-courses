#!/usr/bin/env python3
# *-* coding:utf-8 *-*

"""

:mod:`lab_objects` -- Objects in Python
=========================================

LAB Objects Learning Objective: Explore objects in Python and how everything in Python
                                is an object.

a. Fill in the series of functions below that determine the characteristics of an object.

b. Write a print_object_flags function that uses the is_* functions to find the characteristics
   of the passed in object and print the characteristics (flags).

"""


def is_callable(obj):
    """ returns True if the object is callable """

    # __call__
    methods = dir(obj)
    if "__call__" in methods:
        return True
    else:
        return False


def is_with(obj):
    """ returns True if the object can be used in a "with" context """
    # __enter__, __exit__
    methods = dir(obj)
    if "__enter__" in methods and "__exit__" in methods:
        return True
    else:
        return False


def is_math(obj):
    """ returns True if the object supports +, -, /, and * """
    # __add__, ...
    methods = dir(obj)
    if "__add__" in methods and "__sub__" in methods and "__mul__" in methods:
        return True
    else:
        return False


def is_iterable(obj):
    """ returns True if the object is iterable """
    # __iter__
    methods = dir(obj)
    if "__iter__" in methods:
        return True
    else:
        return False


def print_object_flags(obj):
    """ assess the object for various characteristics and print them """
    print("")
    print(obj)
    print("is callable: {}".format(is_callable(obj)))
    print("is with: {}".format(is_with(obj)))
    print("is math: {}".format(is_math(obj)))
    print("is iterable: {}".format(is_iterable(obj)))


if __name__ == "__main__":
    print_object_flags(1)
    print_object_flags("abc")
    print_object_flags(print_object_flags)
    print_object_flags([1, 2, 3])
    print_object_flags(open('test.file.deleteme', 'w'))
