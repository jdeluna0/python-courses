#!/bin/bash

for account in $(egrep "^[0-9]" tix.txt | sort | uniq);do

jan=$(trendy -ca $account -at -s 01/01/2018 -e 02/01/2018 2>&1 | grep ^Total)
feb=$(trendy -ca $account -at -s 02/01/2018 -e 03/01/2018 2>&1 | grep ^Total)
echo "$account,$jan,$feb"

done
