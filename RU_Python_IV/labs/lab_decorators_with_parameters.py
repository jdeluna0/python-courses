#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

:mod:`lab_decorators_with_parameters` --- Decorators with parameters practice
========================================

a. Modify the decorator you created in the lab_decorators.py to accept an argument
   that specifies whether to print the timing in seconds or milliseconds.  Make the
   default units seconds.

"""
import time

def dec_func_timer(time_accuracy):
    def func_timer(func):
        """Times the passed function"""
        def inner(*args, **kwargs):
            start_time = time.perf_counter()
            a = func(*args, **kwargs)
            stop_time = time.perf_counter()
            elapsed = stop_time - start_time
            print(elapsed)
            return a
        return inner
    return func_timer


@func_timer
def time_me(item):
    """time this function for various calls"""
    def is_prime(num):
        for j in range(2, num):
            if (num % j) == 0:
                return False
        return True

    index = 0
    check = 0
    while index < item:
        check += 1
        if is_prime(check):
            index += 1
    return check


if __name__ == "__main__":
    start = 2000
    for step in range(10):
        # run your decorated function instead
        result = time_me(start)
        print(result)
