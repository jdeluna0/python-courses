#!/usr/bin/env python3
# *-* coding:utf-8 *-*

"""

:mod:`lab_subprocess` -- subprocess module
============================================

LAB subprocess Learning Objective: Familiarization with subprocess

::

 a. Use the subprocess run function to run "ls -l" and print the output.

 b. Do the same as a), but don't print anything to the screen.

 c. Do the same as a), but run the command "/bogus/command". What happens?

 d. Use subprocess run function to run "du -h" and output stdout to a pipe. Read the pipe
    and print the output.

 e. Create a new function commander() which takes in a list of commands to execute
    (as strings) on the arg list, then runs them sequentially printing stdout.

"""

import subprocess

commands = ["ls -l",
            "df -h",
            "whoami",
            "w",
            "du -h ~"]


def commander(commands):
    for command in commands:
        print('\n')
        a = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, text=True)
        # print(a.poll(), a.wait())
        # print(sin)
        if a.args == "du -h ~":
            print("Waiting for du to finish")

        print("$ {}".format(a.args))
        a.wait()
        (sin, serr) = a.communicate()
        print(sin)
        print("Process ID: {}".format(a.pid))


# subprocess.run(['ls', '-l'], stdout=subprocess.DEVNULL, text=True, shell=False)
# subprocess.run(['ls', '-l'], stdout=subprocess.PIPE, text=True, shell=False)

# Print long list of cwd files using list args
# subprocess.run(['ls', '-l'])

# Print long list of cwd files using shell interpreter instead of list args
subprocess.run('ls -l', shell=True)

# Send stdout from ls to /dev/null
subprocess.run(['ls', '-l'], stdout=subprocess.DEVNULL, text=True, shell=False)

# Try to execute bogus command - returns no such file or directory (shell) or
# FileNotFoundError with python list args
subprocess.run('/bogus/command', text=True, shell=True)

print('\n')
a = subprocess.run('du -h', stdout=subprocess.PIPE, text=True, shell=True)
print(a.stdout)

print("\n")
commander(commands)
