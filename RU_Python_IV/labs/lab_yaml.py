#!/usr/bin/env python3
# *-* coding:utf-8 *-*

"""

:mod:`lab_yaml` -- YAML Parsing
=========================================

LAB_YAML Learning Objective: Learn to parse a YAML file using the PyYAML library
                             and use the information.
::

 a. Load the data/widget.yml file using the PyYAML library.

 b. Change the value for the width and height of the window element to be 1/2 their current value.
    Change the size of the text element to be 1/4 it's current value.
    Change the image alignment element to 'justified'.

 c. Save your updated object to widget_updated.yaml using the PyYAML library.

"""

import yaml
# a
yaml_text = open("../data/widget.yml","r").read()
py_obj = yaml.load(yaml_text)

widget = py_obj['widget']
# b1
widget['window']['height'] = int(widget['window']['height'])/2
widget['window']['width'] = int(widget['window']['width'])/2
# b2
widget['text']['size'] = int(widget['text']['size'])/4
# b3
widget['image']['alignment'] = 'justified'
# c

with open('../data/widget_updated.yaml', 'w') as f:
    yaml.dump(py_obj, f)
