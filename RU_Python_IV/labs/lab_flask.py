#!/usr/bin/env python3
# *-* coding:utf-8 *-*

"""

:mod:`lab_flask` -- serving up REST
=========================================

LAB_FLASK Learning Objective: Learn to serve RESTful APIs using the Flask library
::

 a. Using Flask create a simple server that serves the following string for the root route ('/'):
  "<h1>Welcome to my server</h1>"

 b. Add a route for "/now" that returns the current date and time in string format.

 c. Add a route that converts Fahrenheit to Centigrade and accepts the value to convert
    in the url.  For instance, /fahrenheit/32.0 should return "0.0"

 d. Add a route that converts Centigrade to Fahrenheit and accepts the value to convert
    in the url.  For instance, /centigrade/0.0 should return "32.0"

"""
from datetime import datetime
from flask import Flask, request, jsonify

app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def welcome():
    if request.method == 'GET':
        # return "Welcome to my server!\n"
        return "bye", 404
    if request.method == 'POST':
        lang = request.form['language']
        return str(lang)


@app.route('/now')
def hw():
    now = datetime.now()
    a = now.isoformat()
    return "{}\n".format(a)


@app.route('/fahrenheit/<number>')
def f2c(number):
    fahrenheit = float(number)
    centigrade = (fahrenheit - 32) * 5/9
    return "{}\n".format(str(centigrade))


@app.route('/centigrade/<number>')
def c2f(number):
    centigrade = float(number)
    fahrenheit = (centigrade * 9/5) + 32
    return "{}\n".format(str(fahrenheit))


if __name__ == "__main__":
    app.run()
