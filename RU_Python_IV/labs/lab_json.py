#!/usr/bin/env python3
# *-* coding:utf-8 *-*

"""
:mod:`lab_json` -- JSON to YAML and back again
=========================================

LAB_JSON Learning Objective: Learn to navigate a JSON file and convert to a
                             python object.
::

 a. Create a script that expects 3 command line arguments: -j or -y, json_filename, yaml_filename
    The first argument is -j or -y based on whether to convert from JSON to YAML (-j) or
    YAML to JSON (-y)
    The second argument is the name of the json file to parse or save to
    The third argument is the name of the yaml file to parse or save to

 b. Based on the -y/-j selection, parse the contents of the input file using the appropriate
    library.

 c. Using the other library, save the parsed object to the output filename

 d. Test your script using the json and yml files in the data directory.

 e. If you have time, create your own JSON and YAML files and translate between the formats.

"""
import argparse
import json
import yaml
import sys

if __name__ == "__main__":
    usage = "{0}".format(sys.argv[0])
    parser = argparse.ArgumentParser(usage)
    parser.add_argument("-y2j","--yaml2json", dest='yamlcon', help="Convert" + \
                        " yaml to json.", action='store_true', default=False)
    parser.add_argument("-j2y","--json2yaml", dest='jsoncon', help="Convert" + \
                        " json to yaml", action='store_true', default=False)
    parser.add_argument("-jf","--jsonfile", dest='jsonFile', help="Specify" +\
                        " json file", default="")
    parser.add_argument("-yf","--yamlfile", dest='yamlFile', help="Specify" +\
                        " json file", default="")

    args = parser.parse_args()
    # parser.print_help()

    if args.yamlcon and args.jsoncon:
        print("Please choose one conversion.")

    jsonFile = args.jsonFile
    yamlFile = args.yamlFile

    if args.yamlcon:
        with open(yamlFile, 'r') as y:
            yaml_obj = yaml.load(y)

        with open(jsonFile, 'w') as f:
            json.dump(yaml_obj, f)

    if args.jsoncon:
        with open(jsonFile, 'r') as j:
            json_obj = json.load(j)

        # print(json_obj)

        with open(yamlFile, 'w') as f:
            yaml.dump(json_obj, f, default_flow_style=False)
