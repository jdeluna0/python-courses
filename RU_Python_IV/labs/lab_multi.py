#!/usr/bin/env python3
# *-* coding:utf-8 *-*


import multiprocessing
import os
import queue
import threading

import time


"""

:mod:`lab_multi` -- Investigate multiprocessing / multithreading
=========================================

LAB_multi Learning Objective: Learn to use the multiprocessing and multithreading modules
                              to perform parallel tasks.
::

 a. Create set of three functions that perform the following tasks:
    1. Capitalize all strings that come through and pass them along
    2. Count the number of characters in the string and pass along the string and the count as a
       a tuple (string, count).
    3. Check to see if the count is the largest seen so far.  If so, send along a tuple with
       (string, count, True), else send (string, count, False)

 b. Spawn each of those functions into processes (multiprocessing) and wire them together
    with interprocess communications (queues).

 c. Run the entire data/dictionary2.txt file through your processing engine, one word at a time.

 d. In the main process, monitor the results coming from the last stage in the processing engine.
    After all the words have been processed, print the longest word that went through the engine.

 e. If you complete the above tasks, go back and do the same tasks using threads (threading).  Don't
    delete your multiprocessing code, just add the threading code.

"""


def capit(q1, q2):
    while True:
        item = q1.get()
        s_up = item.upper()
        q1.task_done()
        q2.put(s_up)
    pass


def count(q2, q3):
    while True:
        item = q2.get()
        count = len(item)
        tup = (item, count)
        q2.task_done()
        q3.put(tup)
    pass


def save_largest(q3, q4):
    largest = 0
    while True:
        item = q3.get()
        item = item[0]
        if len(item) > largest:
            largest = len(item)
            ret = True
        else:
            ret = False
        tup = (item, len(item), ret)
        q4.put(tup)
        q3.task_done()


if __name__ == "__main__":
    data2 = os.path.join(os.path.dirname(__file__), '../data/dictionary2.txt')

    with open(data2, "r") as f:
        dlist2 = [line.strip() for line in f.readlines()]

    # ------------------------- Multiprocessing START ------------------------
    start_time = time.time()

    # Use top 20 elements instead for testing
    # dlist2 = dlist2[:20]

    q1 = multiprocessing.JoinableQueue()
    q2 = multiprocessing.JoinableQueue()
    q3 = multiprocessing.JoinableQueue()
    q4 = multiprocessing.JoinableQueue()

    p1 = multiprocessing.Process(target=capit, args=[q1, q2])
    p2 = multiprocessing.Process(target=count, args=[q2, q3])
    p3 = multiprocessing.Process(target=save_largest, args=[q3, q4])
    p1.daemon = True
    p2.daemon = True
    p3.daemon = True
    p1.start()
    p2.start()
    p3.start()

    # for item in dlist2:
    #     q1.put(item)
    [q1.put(item) for item in dlist2]

    for item in range(len(dlist2)):
        item = q4.get()
        if item[2] is True:
            most_gigantic_est = item

    completion = time.time() - start_time
    print("Multiprocessing : {} found in {} seconds.".format(
                                            most_gigantic_est[0], completion))

    # ---------------------------- THREADING START ---------------------------

    start_time = time.time()

    q1 = queue.Queue()
    q2 = queue.Queue()
    q3 = queue.Queue()
    q4 = queue.Queue()

    t1 = threading.Thread(target=capit, args=[q1, q2])
    t2 = threading.Thread(target=count, args=[q2, q3])
    t3 = threading.Thread(target=save_largest, args=[q3, q4])

    t1.daemon = True
    t2.daemon = True
    t3.daemon = True
    t1.start()
    t2.start()
    t3.start()

    for item in dlist2:
        q1.put(item)

    for item in range(len(dlist2)):
        item = q4.get()
        if item[2] is True:
            most_gigantic_est = item

    completion = time.time() - start_time
    print("Threading: {} found in {} seconds.".format(
                                        most_gigantic_est[0], completion))
