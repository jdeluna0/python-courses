from __future__ import print_function
from string import punctuation,maketrans
from collections import Counter
from pprint import pprint

book = open('/home/student/Python2_labs/alice_in_wonderland.dat','r').read().lower()
space = ' ' * len(punctuation)
book = book.translate(maketrans(punctuation,space), "'")
book = book.split()

book = Counter(book) 

for i,j in book.most_common(20):
	print('{0:5s}{1:5,d}'.format(i,j))
