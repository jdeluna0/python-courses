
mylist = [(x,(5.0/9.0*(x-32))) for x in range(-40,120,10) if x != 0 and x != 50]
myset = {(x,(5.0/9.0*(x-32))) for x in range(-40,120,10) if x != 0 and x != 50}
mydc = {x:(5.0/9.0*(x-32)) for x in range(-40,120,10)}

print 'This is a list.'
for x,y in mylist:
	print '{0:5d}{1:8.2f}'.format(x,y)

print 'this is a set.'
for x,y in myset:
	print '{0:5d}{1:8.2f}'.format(x,y)

print 'This is a dict.'
for x,y in mydc.items():
	print '{0:5d}{1:8.2f}'.format(x,y)

