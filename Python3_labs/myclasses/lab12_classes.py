""" Initial class exercise """

class BankAccount(object):  # Top tier class (super class)
	acct_cntr = 0
	def __init__(self):   # This method runs during instantiation
        	self.balance = 0  # instance variable 
		BankAccount.acct_cntr += 1
	def __str__(self):
		print '_str_ method entered'
		return 'The balance for this account is ${0:,.2f}'.format(self.balance)
	def withdraw(self, amount):  # a method
        	self.balance -= amount
        	return self.balance
	def deposit(self, amount):     # another method
        	self.balance += amount
        	return self.balance
	def __eq__(self,other):
		print '_eq_method entered:'
		if self.balance == other.balance:
			return True
		return False
	def __del__(self):
		try:
			BankAccount.acct_cntr -= 1
		except:
			print '',				

class BankAccountB(BankAccount):
	def __init__(self, mini, deposit):
		if deposit < mini:
			DepositError.dep = deposit
			DepositError.req = mini
			raise DepositError, 'Deposit too small.'
		else:
			self.balance = 0 + deposit
			self.minimum = mini
			BankAccount.acct_cntr += 1
	def withdraw(self, amount):
		if amount > self.balance:
			print 'Amount greater than balance'
			return self.balance
		self.balance -= amount
		return self.balance
	def __str__(self):
		return 'The balance for this account is ${0:,.2f} and the minimum balance for this account is ${1:,.2f}'.format(self.balance,self.minimum)

class DepositError(Exception):
	pass

