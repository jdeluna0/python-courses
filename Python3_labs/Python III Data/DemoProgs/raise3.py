"""
Create your own exception subclass inheriting from Exception.
You can add data that will be passed to the exception handler.
What magic method must exist in Exception that produces this result?
"""

class DepositError(Exception):
    pass

def deposit1(amt):    
    if amt < 1000:
        ex = DepositError('Deposit too small')
        ex.dep = amt
        ex.req = 1000
        raise ex  # The instance ex is passed to except on a
    else:         # DepositError
        print 'Deposit OK'
    return

try:
    deposit1(100)
except DepositError, err1:
    print err1, '-', err1.dep, '  Need at least', err1.req

