"""Two different ways to use a generator.
         
The first test takes care of the next method and StopIteration for you
by using "for."  It uses the generator as an iterable.  The second
test uses the next method directly and has to test for StopIteration to detect
when the generator has finished.  In this case, the results are the same.
"""

def ev_od(integer, stop):
	while integer < stop:
		if integer % 2 == 0:
			integer += 1
			yield integer -1
		if integer % 2 == 1:
			integer += 1
			yield 'odd'

for z in ev_od(4,15):  # As an iterator
    print z
print 'finished\n'
    
nxt_gen = ev_od(4,15)
while True:   # By directly accessing each item in order
    try:
        print nxt_gen.next()
    except StopIteration:
        print 'finished'
        break
