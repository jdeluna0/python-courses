"""
You may need to change the values of the
ds and di variables to get a meaningful result.
"""

import os, sys

ds = '/home/student/Python3_labs/Python III Data/words.txt' #directory with file
di = '/home/student/Python3_labs'    #directory only
# ds = 'c:/pydata/words.txt'
# di = 'c:/pydata'

print 'Did we get this far?'
print os.getcwd()
print os.path.dirname(ds)
print os.path.basename(ds)
print os.path.exists(di)
print os.path.isdir(di)
print os.path.isfile(ds)
for keys in os.environ:
	print keys, os.environ[keys]
