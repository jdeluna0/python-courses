#import re
from string import punctuation,maketrans
from operator import itemgetter

lost_words = []
wordlist = open('/home/student/Python2_labs/words.txt','r').read().split()
book = open('/home/student/Python2_labs/alice_in_wonderland.dat','r').read().lower()
space = ' ' * len(punctuation)
book = book.translate(maketrans(punctuation,space), "'").split()

word_dict = dict.fromkeys(wordlist, 0)

for words in book:
	if words not in word_dict:
		lost_words.append(words)			
		continue
	word_dict[words] = word_dict[words] + 1

#next lines calculate number of indeividual words in book
total=0 
for key in word_dict:
	if int(word_dict[key]) > 0:
		total += 1
total = total + len(set(lost_words)) 

word_list = zip(word_dict.values(),word_dict.keys())
word_list.sort(reverse=True, key=itemgetter(0))

for value in range(0,20):
	print word_list[value][0],word_list[value][1]

