#import re
from string import punctuation,maketrans

lost_words = []
wordlist = open('/home/student/Python2_labs/words.txt','r').read().split()
book = open('/home/student/Python2_labs/alice_in_wonderland.dat','r').read().lower()
space = ' ' * len(punctuation)
book = book.translate(maketrans(punctuation,space), "'").split()

word_dict = dict.fromkeys(wordlist, 0)

for words in book:
	if words not in word_dict:
		lost_words.append(words)			
		continue
	word_dict[words] = word_dict[words] + 1

word_list = zip(word_dict.values(),word_dict.keys())
word_list.sort()
word_list.reverse()

with open('/home/student/Python3_labs/writefilelab2.txt', 'w') as file1:
# Calculate the percentage of dictionary words used in the book.
	unused_words = word_dict.values().count(0) # number of words with a zero count
	file1.write('\nPercentage of dictionary words used in the book is {0:.2%}\n'.format(float(len(word_dict) - unused_words) / len(word_dict)))
# Remove duplicates from unfound list and sort the result
	unique_unfound = sorted(set(lost_words))
	file1.write('Number of book words not found in the dictionary - {0:d}\n'.format(len(unique_unfound)))
	nuline = 0
# Print each word left justified in 13 character spaces, and
# put five words on a line.
	file1.write('%s %s %s' % ('\n', 'Unfound Words'.center(60), '\n'))
	for word in unique_unfound:
    		file1.write('{0:13s}'.format(word),)  
    	nuline += 1
    	if nuline >= 5:
        	nuline = 0

