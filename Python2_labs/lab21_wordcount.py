import re

not_found = []
word_dict = {}
wordlist = []

words = open('/Users/jame9129/Python2_labs/words.txt','r')
for line in words:
    line = line.lower()
    line = line.strip()
    line = re.sub(r'[^\w]', ' ', line) #removes punctuation
    wordlist.append(line)

for word in wordlist:
	word_dict[word] = 0

words.close()

book = open('/Users/jame9129/Python2_labs/alice_in_wonderland.dat','r')
for line in book:
    line = line.lower()
    line2 = line.replace("--", " ")
    line2 = re.sub("[^a-zA-Z' ]+", '', line2)

    for word in line2.split():
        word = word.strip("\'")
        if word not in word_dict:
            not_found.append(word)
            if (line.find(word) != -1):
                pass
                # print('')
                # print('Word: {0}'.format(word))
                # print('Sentence: {0}'.format(line))
            else:
                pass
                # print('Word: {0}'.format(word))
                # print('Sentence: {0}'.format(line))
            continue
        else:
            word_dict[word] = word_dict[word] + 1

word_list = zip(word_dict.values(),word_dict.keys())
word_list.sort(key=lambda tup: tup[0])
word_list.reverse()

# print("Not found: {0}".format(len(not_found)))
# print(set(not_found))

print('*** Top 10 ***')
print('--------------')
for key,value in word_list[0:10]:
    if key == 0:
        break
    print("{0}:{1}".format(key,value))



book.close()
