from string import whitespace

print 'GDP per capita'
bookin = open('/home/student/Python2_labs/Python II Data/gdp.txt', 'r')
bookin = bookin.read()
bookin = bookin.splitlines()
dict1={}
for row in bookin:
	row = row.split(',')
	percap = (int(row[1])*1000000)/float(row[2])
	dict1[row[0]]=percap
caplist = zip(dict1.values(),dict1.keys())
caplist.sort()
caplist.reverse()
for i,j in caplist:
	print i,j

print
print 'Words,Words,Words'
print
bookin2 = open('/home/student/Python2_labs/Python II Data/split.txt', 'r')
bookin2 = bookin2.read()
for i in bookin2:
	if not i.isalpha() and i not in whitespace : #didn't count numbers, only words
		bookin2 = bookin2.replace(i,'')
bookin2 = bookin2.lower()
bookin2 = bookin2.split()
print len(bookin2), 'words in file.'
set1 = set(bookin2)
print len(bookin2) - len(set1), 'words that are unique.'
print bookin2
