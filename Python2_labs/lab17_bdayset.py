import random

count=0
iters=10000
for iteration in range(iters):
	bday=[]
	for i in range(0,23):
		bday.append(random.randrange(1,366))
#	s = set() 	Alternatively, we can add each element of bday to set s. 
#	for i in bday:
#		s.add(i)
	s=set(bday)	### ...but this is easier.
	if len(bday) > len(s):
		count += 1
print count
print float(count)/(iters)
