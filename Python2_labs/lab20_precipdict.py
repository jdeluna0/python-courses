from itertools import chain

fin = open('/Users/jame9129/Python2_labs/tmpprecip.dat' ,'r')
data = fin.read()
data = data.split()
rain = 0
days = 0
dict1 = {}
dict2 = {}
dict1['1900'] = [0,0,1000,0,0]
dict2['01'] = 0
for linein in data:
	year = linein[4:8]
	month = linein[0:2]
	rainfall = float(linein[8:13])
	temp = int(linein[13:16])
	if year not in dict1:
		dict1[year] = [0,0,1000,0,0]
	dict1[year][4] = dict1[year][4] + 1                        #tracks number of days in year
	dict1[year][0] = float(dict1[year][0]) + rainfall   #tracks sum of rainfall in year
	dict1[year][3] = dict1[year][3] + temp
	if dict1[year][1] < temp:                    #tracks max temperature
		dict1[year][1] = temp
	if dict1[year][2] > temp:                    #tracks min temperature
		dict1[year][2] = temp

	if month not in dict2:
		dict2[month] = 0
	dict2[month] = dict2[month] + rainfall


for year in dict1:
	dict1[year][3] = dict1[year][3] / dict1[year][4]
fin.close()


vlist=zip(dict1.keys(),dict1.values())
vlist2=zip(dict2.keys(),dict2.values())
vlist.sort()
vlist2.sort()

print '{0:5s} | {1:14s} | {2:5s} | {3:5s} | {4:5s}'.format('Year', 'Total Rainfall', 'Max High Temp', 'Min High Temp', 'Average High')
for index in range(len(vlist)):
	print '{0:5s} {1:16.2f} {2:15d} {3:15d} {4:14d}'.format(vlist[index][0], vlist[index][1][0], vlist[index][1][1], vlist[index][1][2], vlist[index][1][3], vlist[index][1][4])

print '{0:5s} {1:5s}'.format('Month','Avg Rainfall')
for index in range(len(vlist2)):
	print '{0:5s} {1:5.2f}'.format(vlist2[index][0],vlist2[index][1] / len(dict1.keys()))


#01|01|1900|00.00|054
#4:8 year
#8:13 rainfall
#13:16 temp		
