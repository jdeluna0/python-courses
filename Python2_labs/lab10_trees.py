tdata=[]
count=0
fin=open('/home/student/Python2_labs/trees.dat', 'r')
for line in fin:
	try:
		line = float(line)
		tdata.append(line)
		count += 1
	except:
		print 'Bad data, %s' % (line)
		continue
print 'Number of trees: %d' % count
print 'Smallest: %.2f' % min(tdata)
print 'Largest: %.2f' % max(tdata)
print 'Average Height: %.2f' % (sum(tdata)/count)
