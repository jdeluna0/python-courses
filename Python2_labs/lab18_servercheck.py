import string
"""
This program creates two sets from the contents of two files by
nesting an open within a list function which is within a set
function.  While this level of nesting is not recommended, it's
good to know it actually works.

The servers set contains a master list of servers to be updated.
The updates set contains the latest set of servers that have been
updated.  The update items are submitted manually by the admin
responsible for the server.  Sometimes, they are not accurate.
"""

updates = open('/home/student/Python2_labs/serverupdates.txt', 'r')
servers = open('/home/student/Python2_labs/servers.txt', 'r')
updates = updates.read()
servers = servers.read()
updates = set(list(updates.splitlines()))
servers = set(list(servers.splitlines()))
#updates = set(open('file', 'r').read().splitlines()) ***one-liner
if updates.issuperset(servers) == False:
	print 'Update list does not exist in master list.'
else:
	print 'Update list is found in master list.'
remaining = updates.difference(servers)
print

print "These are updated servers not found in master list."
#for i in remaining:
#	print i
print '\n'.join(remaining)
print

neededupdates = servers.difference(updates)
validup = servers.intersection(updates)
print len(servers), 'Master server set'
print len(neededupdates), "New master set"
print len(validup), 'Valid updates'

newfile = open('/home/student/Python2_labs/newservers.txt', 'w')
for i in neededupdates:
	newfile.write('%s\n' % i)
newfile.close()

