fin = open("/home/student/Python2_labs", "r")
number_trees = 0
sum_trees = 0 
tall_trees = 0
height = 0
tallest = 0
shortest = 400
while True:
	line = fin.readline()
	if line == "":
		break
	try:
		if int(line) > height and int(line) > tallest:
			tallest = int(line)
		if int(line) < height and int(line) < shortest:
			shortest = int(line)
		height = int(line)
		number_trees += 1
		sum_trees = float(sum_trees + height)	
	except ValueError:
		print "%s is a non-numeric value." % (line.strip())
	if height > 300:
		tall_trees += 1
print "Number of trees: %10d" % number_trees
print "Average height: %11.1f" % float(sum_trees / number_trees)
print "Trees over 300 feet: %6d" % tall_trees
print "The tallest tree: %9d" % tallest
print "The shortest tree: %8d" % shortest

