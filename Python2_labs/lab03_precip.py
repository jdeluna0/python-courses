rainyday = 0
rainfall = 0
fin = open('/home/student/tmpprecip2012.dat' ,'r')
for linein in fin:
	precip = linein[8:13]
	try:
		precip = float(precip)
	except:
		print "%s is invalid data." % precip
		continue
	if precip == 0:
		continue
	else:
		rainyday += 1
		rainfall = rainfall + precip
print 'Number of rainy days: %d' % rainyday
print 'Amount of precipitation for the year: %.2f' % rainfall
